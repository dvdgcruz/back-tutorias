var ng = angular.module( 'angularjs-app', [
  'ngSails',
  'ui.grid',
  'ui.grid.pagination',
  'ui.bootstrap',
  'ngDialog',
  'moment-picker',
  'angucomplete-alt',
] );
