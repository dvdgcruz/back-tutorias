module.exports = {


  friendlyName: 'Borrar',


  description: 'Borrar grupos.',


  inputs: {
    uuid: {
      type: 'string',
      required: true
    }
  },


  exits: {

  },


  fn: async function (inputs, exits) {
    console.log( ' vamos a eliminar->', inputs);
    let grupo = {};
    grupo = await Grupos.update({uuid: inputs.uuid}, {activo: false }).fetch();
    // All done.
    return exits.success(grupo);

  }

};
