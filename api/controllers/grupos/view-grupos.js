module.exports = {


  friendlyName: 'View grupos',


  description: 'Display "Grupos" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/grupos/grupos'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
