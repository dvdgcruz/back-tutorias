module.exports = {


  friendlyName: 'Guardar',


  description: 'Guardar grupos.',


  inputs: {
    uuid: {
      type: 'string',
      required: false
    },
    nombre: {
      type: 'string',
      required: true,
    },
  },


  exits: {

  },


  fn: async function ( inputs, exits ) {
    console.log( ' vamos a guardar->', inputs);
    let grupo = {};
    if (inputs.uuid) {
      grupo = await Grupos.update({uuid: inputs.uuid}, inputs).fetch();
    }else {
      grupo = await Grupos.create( inputs ).fetch();
    }
    // All done.
    return exits.success(periodo);

  }


};
