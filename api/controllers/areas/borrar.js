module.exports = {


  friendlyName: 'Borrar',


  description: 'Borrar areas.',


  inputs: {
    uuid: {
      type: 'string',
      required: true,
    },
  },


  exits: {

  },


  fn: async function (inputs, exits) {
    console.log( ' vamos a eliminar->', inputs);
    let area = {};
    area = await Areas.update({uuid: inputs.uuid}, {activo: false }).fetch();
    // All done.
    return exits.success(area);

  }


};
