module.exports = {


  friendlyName: 'Guardar',


  description: 'Guardar areas.',


  inputs: {
    uuid: {
      type: 'string',
      required: false
    },
    nombre: {
      type: 'string',
      required: true,
    },
  },


  exits: {

  },


  fn: async function ( inputs, exits ) {
    console.log( ' vamos a guardar->', inputs);
    let area = {};
    if (inputs.uuid) {
      area = await Areas.update({uuid: inputs.uuid}, inputs).fetch();
    }else {
      area = await Areas.create( inputs ).fetch();
    }
    // All done.
    return exits.success(area);

  }


};
