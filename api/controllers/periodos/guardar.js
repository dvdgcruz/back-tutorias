module.exports = {


  friendlyName: 'Guardar',


  description: 'Guardar periodos.',


  inputs: {
    uuid: {
      type: 'string',
      required: false
    },
    periodo: {
      type: 'string',
      required: true,
    },
    anio: {
      type: 'string',
      required: true
    }
  },


  exits: {

  },


  fn: async function ( inputs, exits ) {
    console.log( ' vamos a guardar->', inputs);
    let periodo = {};
    if (inputs.uuid) {
      periodo = await Periodos.update({uuid: inputs.uuid}, inputs).fetch();
    }else {
      periodo = await Periodos.create( inputs ).fetch();
    }
    // All done.
    return exits.success(periodo);

  }


};
