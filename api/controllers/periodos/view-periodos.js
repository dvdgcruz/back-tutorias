module.exports = {


  friendlyName: 'View periodos',


  description: 'Display "Periodos" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/periodos/periodos'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
