module.exports = {


  friendlyName: 'Borrar',


  description: 'Borrar periodos.',


  inputs: {
    uuid: {
      type: 'string',
      required: true
    }
  },


  exits: {

  },


  fn: async function (inputs, exits) {
    console.log( ' vamos a eliminar->', inputs);
    let periodo = {};
    periodo = await Periodos.update({uuid: inputs.uuid}, {activo: false }).fetch();
    // All done.
    return exits.success(periodo);

  }


};
