module.exports = {


  friendlyName: 'Borrar',


  description: 'Borrar carreras.',


  inputs: {
    uuid: {
      type: 'string',
      required: true,
    },
  },


  exits: {

  },


  fn: async function (inputs, exits) {
    console.log( ' vamos a eliminar->', inputs);
    let carrera = {};
    carrera = await Carreras.update({uuid: inputs.uuid}, {activo: false }).fetch();
    // All done.
    return exits.success(carrera);

  }


};
