module.exports = {


  friendlyName: 'View carreras',


  description: 'Display "Carreras" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/carreras/carreras'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
