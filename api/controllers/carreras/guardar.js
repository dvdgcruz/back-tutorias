module.exports = {


  friendlyName: 'Guardar',


  description: 'Guardar carreras.',


  inputs: {
    uuid: {
      type: 'string',
      required: false
    },
    nombre: {
      type: 'string',
      required: true,
    },
  },


  exits: {

  },


  fn: async function ( inputs, exits ) {
    console.log( ' vamos a guardar->', inputs);
    let carrera = {};
    if (inputs.uuid) {
      carrera = await Carreras.update({uuid: inputs.uuid}, inputs).fetch();
    }else {
      carrera = await Carreras.create( inputs ).fetch();
    }
    // All done.
    return exits.success(carrera);

  }


};
