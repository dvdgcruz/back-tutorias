module.exports = {


  friendlyName: 'Borrar',


  description: 'Borrar usuarios.',


  inputs: {
    uuid: {
      type: 'string',
      required: true
    }
  },


  exits: {

  },


  fn: async function (inputs, exits) {
    console.log( ' vamos a eliminar->', inputs);
    let usuario = {};
    usuario = await Usuarios.update({uuid: inputs.uuid}, {activo: false }).fetch();
    // All done.
    return exits.success(usuario);

  }


};
