module.exports = {


  friendlyName: 'View usuarios',


  description: 'Display "Usuarios" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/usuarios/usuarios'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
