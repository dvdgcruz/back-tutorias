module.exports = {


  friendlyName: 'Guardar',


  description: 'Guardar usuarios.',


  inputs: {
    uuid: {
      type: 'string',
      required: false
    },
    nombre: {
      type: 'string',
      required: true,
    },
    matricula: {
      type: 'string',
      required: true,
    },
    password: {
      type: 'string',
      required: true,
    },
    perfilName: {
      type: 'string',
      required: true,
    }
  },


  exits: {
    success: {
      description: 'The requesting user agent has been successfully created.',
    },
  },


  fn: async function ( inputs, exits ) {
    console.log( ' vamos a guardar->', inputs);
    let usuario = {};
    if (inputs.uuid) {
      usuario = await Usuarios.update({uuid: inputs.uuid}, inputs).fetch();
    }else {
      usuario = await Usuarios.create( inputs ).fetch();
    }
    // All done.
    return exits.success(usuario);

  }


};
