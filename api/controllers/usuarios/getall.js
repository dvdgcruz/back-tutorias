module.exports = {


  friendlyName: 'Getall',


  description: 'Getall usuarios.',


  inputs: {

  },


  exits: {

  },


  fn: async function ( inputs, exits ) {

    console.log('USUARIOS->GETALL->');
    let usuarios = await Usuarios.find({ activo: true});
    // All done.
    return exits.success( usuarios );

  },


};
