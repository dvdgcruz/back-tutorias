module.exports = {


  friendlyName: 'Gettutores',


  description: 'Gettutores usuarios.',


  inputs: {
    id: {
      type: 'number'
    }
  },


  exits: {

  },


  fn: async function ( inputs, exits ) {
    // const director = await Usuarios.findOne({id: inputs.id});
    let tutores = await Usuarios.find({perfilName: 'tutor'});
    // All done.
    return exits.success(tutores);

  }


};
