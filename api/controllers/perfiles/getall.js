module.exports = {


  friendlyName: 'Getall',


  description: 'Getall perfiles.',


  inputs: {

  },


  exits: {

  },


  fn: async function ( inputs, exits ) {

    let perfiles = await Perfiles.find({ visible: true});
    // All done.
    return exits.success( perfiles );

  }


};
