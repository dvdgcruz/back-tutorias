module.exports = {


  friendlyName: 'Borrar',


  description: 'Borrar alumnos.',


  inputs: {
    uuid: {
      type: 'string',
      required: true,
    },
  },


  exits: {

  },


  fn: async function (inputs, exits) {
    console.log( ' vamos a eliminar->', inputs);
    let alumno = {};
    alumno = await Alumnos.update({uuid: inputs.uuid}, {activo: false }).fetch();
    // All done.
    return exits.success(alumno);

  }


};
