module.exports = {


  friendlyName: 'Guardar',


  description: 'Guardar alumnos.',


  inputs: {
    uuid: {
      type: 'string',
      required: false,
    },
    matricula: {
      type: 'string',
      required: true,
    },
    nombre: {
      type: 'string',
      required: true,
    },
    apellioPaterno: {
      type: 'string',
      required: true,
    },
    apellidoMaterno: {
      type: 'string',
      required: true,
    },
    curp: {
      type: 'string',
      required: true,
    },
    nss: {
      type: 'string',
      required: true,
    },
    correo: {
      type: 'string',
      required: true,
    },
    telefono: {
      type: 'string',
      required: true,
    },
  },


  exits: {

  },


  fn: async function ( inputs, exits ) {
    console.log( ' vamos a guardar->', inputs);
    let alumno = {};
    if (inputs.uuid) {
      alumno = await Alumnos.update({uuid: inputs.uuid}, inputs).fetch();
    }else {
      alumno = await Alumnos.create( inputs ).fetch();
    }
    // All done.
    return exits.success(alumno);

  }

};
