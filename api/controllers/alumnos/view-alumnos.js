module.exports = {


  friendlyName: 'View alumnos',


  description: 'Display "Alumnos" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/alumnos/alumnos'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
