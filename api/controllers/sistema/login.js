
module.exports = {


  friendlyName: 'Login',


  description: 'Login sistema.',


  inputs: {
    matricula: {
      type: 'string',
      required: true,
    },
    password: {
      type: 'string',
      required: true,
    },
  },


  exits: {
    success: {
      description: 'The requesting user agent has been successfully logged in.',
    },
  },


  fn: async function (inputs, exits) {
    let userRecord = await Usuarios.findOne({
      matricula: inputs.matricula.toLowerCase(),
      password: inputs.password.toLowerCase(),
      activo: true,
    });

    console.log( userRecord );
    // If there was no matching user, respond thru the "badCombo" exit.
    if (!userRecord) {
      throw 'badCombo';
    }

    // Modify the active session instance.
    this.req.session.userUuid = userRecord.uuid;
    let token = await jwtService.issue( userRecord );

    // mandar un socket...
    sails.sockets.broadcast('app', 'login', userRecord.fullName);

    let sanitizedFields = [
      'password',
      'createdAt',
      'updatedAt',
      'activo',
    ];

    userRecord = _.omit(userRecord, sanitizedFields);
    // All done.
    return exits.success({ usuario: userRecord, token: token });
  }


};
