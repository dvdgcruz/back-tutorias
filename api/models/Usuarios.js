/**
 * Usuarios.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
var uuidv4 = require( 'uuid/v4' );

module.exports = {

  attributes: {
    uuid: {
      type: 'string',
      allowNull: true,
    },
    nombre: {
      type: 'string',
      required: true,
    },
    matricula: {
      type: 'string',
      required: true,
    },
    password: {
      type: 'string',
      required: true,
    },
    permisos: {
      type: 'json',
      defaultsTo: [],
    },
    perfilName: {
      type: 'string',
      required: true,
      isIn: [
        'super',
        'admin',
        'coord',
        'direc',
        'tutor',
      ],
      description: 'Nombre del perfil',
    },
    isSuper: {
      type: 'boolean',
      defaultsTo: false,
    },
    activo: {
      type: 'boolean',
      defaultsTo: true,
    },
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    perfil: {
      model: 'perfiles',
      description: 'Relacion con el perfil asignado al crear la cuenta.',
    },
  },
  beforeCreate: async function( req, next ) {
    req.uuid = uuidv4();
    req.matricula = req.matricula.toLowerCase();

    var found = await Usuarios.findOne( { where: { matricula: req.matricula, }, } );
    if ( found ) { return next( 'Usuario Duplicado' ); }

    // el usuario se puede crear siempre y cuando exista el perfil
    // se crea con los permisos solicitados o con los que contiene la definicion
    // del perfil
    var perfil = await Perfiles.findOne( { nombre: req.perfilName, } );
    if ( !perfil ) { return next( 'No existe el perfil que quiere crear ' + req.perfilName ); }
    let permisos = perfil.permisos;
    if ( req.permisos && req.permisos.length ) { permisos = req.permisos; }
    req.permisos = permisos;
    req.perfil = perfil.id;

    return next();

  },

};

