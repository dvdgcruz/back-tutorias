/**
 * Grupos.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

var uuidv4 = require( 'uuid/v4' );


module.exports = {

  attributes: {
    uuid: {
      type: 'string',
      allowNull: true,
      description: 'Id del grupo perteneciente a una carrera y este perteneciente a un área',
    },
    nombre: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 50,
      description: 'Nombre del grupo',
      example: 'TI-102(identificador de carrera segui de un "-" seguido del nivel de grupo )',
    },
    activo: {
      type: 'boolean',
      defaultsTo: true,
    },
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    carrera: {
      model: 'carreras',
    },
    area: {
      model: 'areas',
    },
  },

  beforeCreate: async function( req, next ) {
    req.uuid = uuidv4();
    req.nombre = req.nombre.toUpperCase();

    var found = await Grupos.findOne( { where: { nombre: req.nombre, }, } );
    if ( found ) { return next( 'Grupo Duplicado' ); }

    // el grupo se puede crear siempre y cuando exista carreras y areas
    var carrera = await Carreras.findOne( { nombre: req.carrera.nombre, } );
    if ( !carrera ) { return next( 'No existe la carrera que quiere crear ' + req.carrera.nombre ); }
    
    var area = await Areas.findOne( { nombre: req.area.nombre, } );
    if ( !area) { return next( 'No existe el area que quiere crear ' + req.area.nombre ); }
    

    return next();

  },
};

