/**
 * Alumnos.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

var uuidv4 = require( 'uuid/v4' );

module.exports = {

  attributes: {

    uuid: {
      type: 'string',
      allowNull: true,
      description: 'Id de un alumno',
    },
    matricula: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 20,
      description: 'Matricula del alumno',
      example: 'UTTI162003 | UTTG234001',
    },
    nombre: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 20,
      description: 'Nombre del alumno',
      example: 'Edgar Zuriel',
    },
    apellioPaterno: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 20,
      description: 'Apellido paterno del alumno',
      example: 'Zarate',
    },
    apellidoMaterno: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 20,
      description: 'Apellido materno del alumno',
      example: 'Montesinos',
    },
    curp: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 20,
      description: 'CURP del alumno',
      example: 'ZAME910812HOCRND01'
    },
    nss: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 20,
      description: 'Numero de seguro social del alumno',
      example: '13224687431358',
    },
    correo: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 50,
      description: 'Correo del alumno',
      example: 'mdantez666@gmail.com',
    },
    telefono: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 10,
      description: 'Matricula del alumno',
      example: '9512267176',
    },
  },
    activo: {
      type: 'boolean',
      defaultsTo: true,
    },

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    carrera: {
      model: 'carreras',
    },
    grupo: {
      model: 'grupos',
    },
    periodos: {
      model: 'periodos'
    },
    area: {
      model: 'areas'
    },
    beforeCreate: async function( req, next ) {
      req.uuid = uuidv4();
      req.matricula = req.matricula.toUpperCase();
  
      var found = await Alumnos.findOne( { where: { matricula: req.matricula, }, } );
      if ( found ) { return next( 'Alumno Duplicado' ); }
  
      // el alumno se puede crear siempre y cuando exista carreras, grupo, periodos, areas
      var carrera = await Carreras.findOne( { nombre: req.carrera.nombre, } );
      if ( !carrera ) { return next( 'No existe la carrera que quiere crear ' + req.carrera.nombre ); }
      
      var area = await Areas.findOne( { nombre: req.area.nombre, } );
      if ( !area) { return next( 'No existe el area que quiere crear ' + req.area.nombre ); }
      
      var grupo = await Grupos.findOne( { nombre: req.grupo.nombre, } );
      if ( !grupo) { return next( 'No existe el grupo que quiere crear ' + req.grupo.nombre ); }
      
      var periodo = await Periodos.findOne( { nombre: req.periodo.nombre, } );
      if ( !periodo) { return next( 'No existe el periodo que quiere crear ' + req.periodo.nombre ); }
      
  
      return next();
  
    },

  };

