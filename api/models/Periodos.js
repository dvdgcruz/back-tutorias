/**
 * Periodos.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

var uuidv4 = require( 'uuid/v4' );

module.exports = {

  attributes: {
    uuid: {
      type: 'string',
      allowNull: true,
      unique: true,
      description: 'Id del periodo',
    },
    periodo: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 50,
      description: 'Nombre del cuatrimestre (periodo) dado de alta',
      example: 'enero-mayo',
    },
    anio: {
      type: 'string',
      required: true,
      description: 'Anio del periodo dado de alta',
      example: '2020',
    },
    activo: {
      type: 'boolean',
      defaultsTo: true,
    },
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    carrera: {
      model: 'carreras',
    },
    grupo: {
      model: 'grupos',
    },
    alumno: {
      model: 'alumnos',
    },
    usuario: {
      model: 'usuarios'
    },
  },
  beforeCreate: async function( req, next ) {
    req.uuid = uuidv4();

    var found = await Periodos.findOne( { where: { uuid: req.uuid, }, } );
    if ( found ) { return next( 'Periodo Duplicado' ); }

    return next();

  },

};

