/**
 * Carreras.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

var uuidv4 = require( 'uuid/v4' );


module.exports = {

  attributes: {

    uuid: {
      type: 'string',
      allowNull: true,
      description: 'Id de una carrera',
    },
    nombre: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 50,
      description: 'Nombre de la carrera',
      example: 'Ing. en Mecatronica | T.S.U en Mecatronica',
    },
    activo: {
      type: 'boolean',
      defaultsTo: true,
    },
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
  },
  beforeCreate: async function( req, next ) {
    req.uuid = uuidv4();
    req.nombre = req.nombre.toLowerCase();

    var found = await Carreras.findOne( { where: { nombre: req.nombre, }, } );
    if ( found ) { return next( 'Carrera Duplicado' ); }
    
    return next();

  },

};

