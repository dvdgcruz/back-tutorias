/**
 * Areas.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
var uuidv4 = require( 'uuid/v4' );

module.exports = {

  attributes: {

    uuid: {
      type: 'string',
      allowNull: true,
      description: 'Id de un area',
    },
    nombre: {
      type: 'string',
      required: true,
      unique: true,
      maxLength: 50,
      description: 'Nombre del area',
      example: 'Sistemas informaticos | Tedes | Telecomunicaciones',
    },
    activo: {
      type: 'boolean',
      defaultsTo: true,
    },
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    carrera: {
      model: 'carreras',
    },
  },
  beforeCreate: async function( req, next ) {
    req.uuid = uuidv4();
    req.nombre = req.nombre.toUpperCase();

    // el area se puede crear siempre y cuando existan carreras
    var carrera = await Carreras.findOne( { nombre: req.carrera.nombre, } );
    if ( !carrera ) { return next( 'No existe la carrera que quiere crear ' + req.carrera.nombre ); }
    
    

    return next();

  },

};

