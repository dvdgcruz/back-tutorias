require( 'dotenv' ).config();
module.exports = {
  friendlyName: 'Ensure database',
  description: '',
  inputs: {
  },
  exits: {
    success: {
      description: 'All done.',
    },
  },

  fn: async function (inputs, exits) {

    // verificar que existan los permisos que usamos
    if ( !await Permisos.count() && sails.config.colecciones && sails.config.colecciones.permisos.length ) {
      sails.log.verbose( '◊ Generando permisos.' );
      await Permisos.createEach( sails.config.colecciones.permisos );
    }
    // generar perfiles (si es que no vienen en el script de inicializacion y hay en la configuracion)
    if ( !await Perfiles.count() && sails.config.colecciones && sails.config.colecciones.perfiles.length ) {
      sails.log.verbose( '◊ Generando perfiles.' );
      await Perfiles.createEach( sails.config.colecciones.perfiles );
    }

    if ( !await Usuarios.count() && sails.config.colecciones && sails.config.colecciones.usuarios.length ) {
      for ( const u in sails.config.colecciones.usuarios ) {
        var user = sails.config.colecciones.usuarios[u];
        // si no existe...
        if ( await Usuarios.count( { matricula: user.matricula, } ) === 0 ) {
          sails.log.verbose( `◊ Generando usuario: ${user.matricula}, contraseña: ${process.env.FIRST_PASSWORD}` );
          // encriptar password...
          user.password = process.env.FIRST_PASSWORD;
          // user.password = await sails.helpers.passwords.hashPassword( process.env.FIRST_PASSWORD );
          // crearlo
          await Usuarios.create( user ).fetch();
        }
      }
    }

    sails.log.verbose( `★ Verificación finalizada...` );
    return exits.success();
  },


};

