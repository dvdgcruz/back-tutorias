require( 'dotenv' ).config();
var jwt = require( 'jsonwebtoken' );

module.exports = {
  friendlyName: 'Ensure database',
  description: '',
  inputs: {
    payload: {
      type: 'json',
      required: true,
    },
  },
  exits: {
  },

  fn: async function (inputs) {

    return jwt.sign(
        inputs.payload, // This is the payload we want to put inside the token
        sails.config.custom.jwtSecret // Secret string which will be used to sign the token
    );
  },


};
