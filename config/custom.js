/**
 * Custom configuration
 * (sails.config.custom)
 *
 * One-off settings specific to your application.
 *
 * For more information on custom configuration, visit:
 * https://sailsjs.com/config/custom
 */
require( 'dotenv' ).config();
const fs = require( 'fs' );
let rawdata = fs.readFileSync( 'package.json' );
let package = JSON.parse( rawdata );
module.exports.custom = {
  app: {
    name: package.description,
    version: package.version,
    author: package.author,
  },
  jwtSecret: process.env.JWT_SECRET,
  passwordResetTokenTTL: 24, // hours
  emailProofTokenTTL: 24, // hours

  /***************************************************************************
  *                                                                          *
  * Any other custom config this Sails app should use during development.    *
  *                                                                          *
  ***************************************************************************/
  // mailgunDomain: 'transactional-mail.example.com',
  // mailgunSecret: 'key-testkeyb183848139913858e8abd9a3',
  // stripeSecret: 'sk_test_Zzd814nldl91104qor5911gjald',
  // …
  rememberMeCookieMaxAge: 30 * 24 * 60 * 60 * 1000, // 30 days
};
