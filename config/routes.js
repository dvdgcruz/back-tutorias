/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/
  /*
 ██╗   ██╗██╗███████╗████████╗ █████╗ ███████╗
 ██║   ██║██║██╔════╝╚══██╔══╝██╔══██╗██╔════╝
 ██║   ██║██║███████╗   ██║   ███████║███████╗
 ╚██╗ ██╔╝██║╚════██║   ██║   ██╔══██║╚════██║
  ╚████╔╝ ██║███████║   ██║   ██║  ██║███████║
   ╚═══╝  ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝
*/
  'GET /':                          { action: 'sistema/view-login' },
  'GET /home':                      { action: 'sistema/view-home' },
  'GET /grupos':                    { action: 'grupos/view-grupos' },
  'GET /carreras':                  { action: 'carreras/view-carreras' },
  'GET /alumnos':                   { action: 'alumnos/view-alumnos' },
  'GET /periodos':                  { action: 'periodos/view-periodos' },
  'GET /usuarios':                  { action: 'usuarios/view-usuarios' },



  'POST /api/v1/sistema/login':     { action: 'sistema/login' },
  'GET /api/v1/usuarios/getall':    { action: 'usuarios/getall' },
  'POST /api/v1/usuarios/guardar':  { action: 'usuarios/guardar' },
  'POST /api/v1/usuarios/gettutores': { action: 'usuarios/gettutores' },
  'GET /api/v1/perfiles/getall':    { action: 'perfiles/getall' },
  'POST /api/v1/alumnos/guardar':   { action: 'alumnos/guardar' },
  'POST /api/v1/carreras/guardar':  { action: 'carreras/guardar' },
  'POST /api/v1/grupos/guardar':    { action: 'grupos/guardar' },
  'POST /api/v1/periodos/guardar':  { action: 'periodos/guardar' },
  'POST /api/v1/usuarios/borrar': { action: 'usuarios/borrar' },
  'POST /api/v1/periodos/borrar': { action: 'periodos/borrar' },
  'POST /api/v1/areas/guardar': { action: 'areas/guardar' },
  'POST /api/v1/grupos/borrar': { action: 'grupos/borrar' },
  'POST /api/v1/carreras/borrar': { action: 'carreras/borrar' },
  'POST /api/v1/areas/borrar': { action: 'areas/borrar' },
  'POST /api/v1/alumnos/borrar': { action: 'alumnos/borrar' },


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
